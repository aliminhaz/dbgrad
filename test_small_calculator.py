from small_calculator import SmallCalculator
class TestCalculator:

    def test_increment_by_one(self):
        #arrange
        initial_value = 1
        calculator = SmallCalculator(initial_value)

        #Act
        result = calculator.increment_by_one()

        #assert
        assert result == 2

    def test_increment_by_one_from_negative(self):
        initial_value = -1
        calculator = SmallCalculator(initial_value)
        result = calculator.increment_by_one()
        assert result == 0

    def test_wrong(self):
        initial_value = -1
        calculator = SmallCalculator(initial_value)
        result = calculator.increment_by_one()
        assert result == -2

#the main trick
if __name__ == "__main__":
    print (__name__)
    test_calculator = TestCalculator()
    test_calculator.test_increment_by_one()
    test_calculator.test_increment_by_one_from_negative()
    test_calculator.test_wrong()
